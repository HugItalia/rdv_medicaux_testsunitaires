<?php

namespace App\Repository;

use App\Entity\TypeConsultation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TypeConsultation|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeConsultation|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeConsultation[]    findAll()
 * @method TypeConsultation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeConsultationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TypeConsultation::class);
    }

    // /**
    //  * @return TypeConsultation[] Returns an array of TypeConsultation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeConsultation
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
